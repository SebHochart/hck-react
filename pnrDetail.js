var PnrDetail = React.createClass({
    show: function() {
        var e = document.getElementById("main-content");
        e.style.display = 'block';
    },
    onClick: function() {
        var e = document.getElementById("main-content");
        e.style.display = 'none';
    },
    render: function() {
        return (
                <div>
                    <table>
                      <thead>
                          <tr>
                            <td>
                              <a href="#" onClick={this.show} >Detail</a>
                              <a href="#" onClick={this.onClick} > Close </a>
                            </td>
                          </tr>
                      </thead>
                    </table>
                    <div id="main-content">
                        <table>
                          <tbody>
                              <tr>
                                  <td>PNR:</td>
                                  <td>{this.props.data.pnr}</td>
                              </tr>
                              <tr>
                                  <td>Name:</td>
                                  <td>{this.props.data.lastName} / {this.props.data.firstName}</td>
                              </tr>
                              <tr>
                                  <td>Itinerary:</td>
                                  <td>{this.props.data.CarrierCode} {this.props.data.FlightNum} {this.props.data.DepartureDate}  {this.props.data.Origin}-{this.props.data.Destination}</td>
                              </tr>
                              <tr>
                                  <td>SSRs:</td>
                                  <td>{this.props.data.SSR}</td>
                              </tr>
                              <tr>
                                  <td>Tickets:</td>
                                  <td>{this.props.data.TicketNum}</td>
                              </tr>
                              <tr>
                                  <td>EMDs:</td>
                                  <td>{this.props.data.EMD}</td>
                              </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
        );
    }
});