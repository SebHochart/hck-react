
// Raw service data
var service = {

	wsOffersUrl:"http://services.stage.plusgrade.com:8600/partner-offers",
	ranklistEndpoint:"/service/rankedList/partners/179/products/10048145/travelDate/2017-07-02/upgradeType/BUSINESS?operatingCurrency=CAD",

	getRanklist:function(callback) {

		var url = "http://localhost:8000/ws/ranklist.xml"; //service.wsOffersUrl + service.ranklistEndpoint;

        function getLoyaltyLevelString( loyaltyLevelInt ) {
            switch ( loyaltyLevelInt )
            {
                case "1":
                    return "Classic Member";
                case "2":
                    return "Silver Member";
                case "3":
                    return "Gold Member";
                case "4":
                    return "Platinum Member";
                case "0":
                default:
                    return "";
            }
        }

		util.doAjax("GET", url, function( xmlData ) {

            var x2js = new X2JS( {
            	arrayAccessFormPaths : [
            		"RankListResponse.OfferCounts.NumberOfOffers",
					"RankListResponse.RankedOffers.RankedOffer"
				],
                attributePrefix : "$"
			});
            var jsonObj = x2js.xml_str2json( xmlData );
            

            var rankedOfferList = [ ], i;

            for ( i = 0; i< jsonObj.RankListResponse.RankedOffers.RankedOffer.length; i++ )
            {
                var currentOffer = jsonObj.RankListResponse.RankedOffers.RankedOffer[ i ];

                rankedOfferList.push(
                    {
                        // Ranked Offer Data

                        "rank": currentOffer.$Rank,
                        "offerId": currentOffer.$Id,
                        "selected": currentOffer.$Selected == 'true',
                        "rejected": currentOffer.$Rejected == 'true',
                        "pruned": currentOffer.$Pruned == 'true',
                        "pnr": currentOffer.ConfirmationNumber,
                        "lastName": currentOffer.LastName,
                        "score": currentOffer.TotalScore.__text,
                        "paxCount": currentOffer.Pax,
                        "bid": {
                            "currencyCode": currentOffer.Bid.Amount.$CurrencyCode,
                            "amountPerPassenger": currentOffer.Bid.Amount.__text,
                            "quantity": currentOffer.Bid.Quantity,
                            "totalAmount": currentOffer.Bid.Quantity * currentOffer.Bid.Amount
                        },
                        "loyaltyLevel": getLoyaltyLevelString( currentOffer.LoyaltyStatusLevel ),
                        "originalFareClass": currentOffer.FareClass,
                        "successRatio": {
                            "successfulCount": currentOffer.CustomerSuccessRatio.SuccessfulCount,
                            "totalCount": currentOffer.CustomerSuccessRatio.TotalCount,
                            "ratio": currentOffer.CustomerSuccessRatio.Ratio
                        },

                        // PNR Detail Data

                        "firstName": "JOHN MICHAEL",
					    "CarrierCode": "AC",
					    "FlightNum"  : "123",
					    "DepartureDate": "11 JUN",
					    "Origin": "JFK",
					    "Destination":"LAX",
					    "SSR": "VGM/WCHC",
					    "TicketNum": "001-1234567890",
					    "EMD": "001-8234567890"

                    }
                );
            }

            callback(rankedOfferList);
		});

	}
}

// util 
var util ={

	doAjax:function(action, url, success) {
		var r = new XMLHttpRequest();
		r.open(action, url, true);
		r.onreadystatechange = function () {
		  if (r.readyState != 4 || r.status != 200) return;
		  success(r.responseText);
		};
		r.send(url);
	}
}


// Data transform for React components usage
var api = {

	getRankedOffers:function(callback){

		service.getRanklist(callback);
	}
  
}

