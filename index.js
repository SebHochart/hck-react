var OfferList = React.createClass({
  render: function() {

    var offers = [];
    for (var ii = 0; ii < this.props.offerList.length; ii++) {
      offers.push(
        <Offer key={ii} data={this.props.offerList[ii]} click={this.props.click}/>
      );
    }

    return (
      <table className="table component-rank-list">
        <thead>
          <tr>
            <th>Rank</th>
            <th>PNR</th>
            <th>Last Name</th>
            <th>Bid amount</th>
            <th>Pax Count</th>
            <th>Total Amount</th>
          </tr>
        </thead>

        <tbody>
        
        {offers}

        </tbody>
      </table>
    );
  }

}); 


var App = React.createClass({

  getInitialState: function() {
    return {
      offerList: []
    }
  },

  onClickPnr: function(e, pnrData) {

    ReactDOM.render(
      <PnrDetail data={pnrData}/>,
      document.getElementById('detailContainer')
    );
  
    e.stopPropagation();
  },

  onClickUpgrade: function(e) {

    $('#message').html("");

    var message = 'Processing upgrade of : '
      $('.offer.selected').each(function(index, element) {
      var pnr = $(element).find('.pnr').text();
      message += pnr + " ";
    });

    $('#message').html( message );

    setTimeout(function(){ 
      $('#message').html( "upgrade successful !" );
    }, 2000);

    setTimeout(function(){ 
      $('#message').html( "" );
    }, 4000);

   },

  componentDidMount: function() {

    var that = this;
    api.getRankedOffers( function(data) {
      that.setState({
        offerList: data
      })
    });
  },

  render: function() {

    return (
        <div>
        <nav className="navbar navbar-default navbar-light" role="navigation">
            <div className="navbar-header">
                <div>
                    <img id="logo" src="/images/AC_logo.png"/>
                </div>
            </div>
            <div>
                <ul className="nav navbar-nav">
                    <li className="active"><a href="#">Flights &amp; Offers</a></li>
                    <li><a href="#" className="navbar-link navbar-left">Ticketing</a></li>
                    <li><a href="#" className="navbar-link navbar-left">Reports &amp; Intelligence</a></li>
                    <li><a href="#" className="navbar-link navbar-left">Settings</a></li>
                </ul>
                <ul className="nav navbar-nav navbar-right">
                    <li><a href="#" className="navbar-link">Tai Pham</a></li>
                    <li><a href="#" className="navbar-link">Sign out</a></li>
                    <li><img id="logo" src="/images/PG_logo.png"/></li>
                </ul>
              </div>
            <p className = "navbar-text navbar-right">
                <a href="#" className="navbar-link">Tai Pham</a>
            </p>
        </nav>

        <div className="row flight-summary">
          <Summary />
        </div>

        <div className="row">
            <div className="col-md-9" id="offerList">
                <OfferList offerList={this.state.offerList} click={this.onClickPnr} />
                <button type="button" className="btn btn-primary pull-right" onClick={this.onClickUpgrade}>Upgrade selected offers</button>
                <div id="message" className="pull-left"></div>
            </div>

            <div className="col-md-3">
                <div id="detailContainer" className="text-center row component-pnr-details">
                </div>
            </div>
        </div>
      
        </div>
    );
  }

}); 

var Summary = React.createClass({
  render: function() {
    return (
    <div>
      <div className="col-md-4 flight-info">
        <b>Flight Summary</b>
        <div className="row">Flight: AC 123</div>
        <div className="row">Departure Date: 10 June 2017</div>
        <div className="row">Destination-Origin: JFK-LAX</div>
      </div>
      <div className="col-md-4 flight-info">
        <b>Capacity Details</b>
        <div className="row">Capacity: 50</div>
        <div className="row">Available: 40</div>
        <div className="row">Allocated: 20</div>
      </div>
      <div className="col-md-4 flight-info">
        <b>Potential Revenue</b>
        <div className="row"><h2>$9,230</h2></div>
      </div>
      </div>
    );
  }
});

ReactDOM.render(
  <App />,
  document.getElementById('container')
);

