var Offer = React.createClass({
  getInitialState: function() {
    return {
      selected: false
    }
  },

  clickMe: function() {
    var isSelected = !this.state.selected;

    this.setState({
      selected:isSelected 
    });

    this.forceUpdate()
  },

  componentDidMount: function() {
    this.setState({
      selected: this.props.data.selected 
    });
  },

  render: function() {
    var data = this.props.data;

    var selectedClass = this.state.selected ? "offer selected" : "offer";

    return (
      <tr onClick={this.clickMe} className={selectedClass}>
        <td>{this.props.data.rank}</td>
        <td><button className="btn pnr" onClick={(e) => this.props.click(e, data)}> {this.props.data.pnr} </button></td>
        <td>{this.props.data.lastName}</td>
        <td>{this.props.data.bid.currencyCode} {this.props.data.bid.amountPerPassenger} /pax</td>
          <td>{this.props.data.paxCount}</td>
          <td>{this.props.data.bid.currencyCode} {this.props.data.bid.totalAmount}</td>
      </tr>
    );
  }

}); 
